import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Project} from "../interfaces/project";
/**
 * @author Damian Rabczewski,
 */
@Injectable()
export class ProjectService {

  constructor(public http: HttpClient) {
  }


  public getProjectAll() {
    return this.http.get('http://localhost:8080/employee/project/findAll', {}
    );
  }

  public getProjectDetail(id: number) {
    return this.http.get('http://localhost:8080/employee/project/findOne?id=:id', {}
    );
  }

  public deleteProject(id: number) {
    return this.http.delete('http://localhost:8080/employee/project/deleteOne?id=:id', {}
    );
  }

  public saveProject(project: Project) {
    project.status = 'MAINTENANCE';
    return this.http.post('http://localhost:8080/employee/project/save', JSON.stringify(project), {headers:{'Content-Type': 'application/json'}}
    );
  }

}
