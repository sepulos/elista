import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Hobby} from "../interfaces/hobby";
/**
 * @author Damian Rabczewski,
 */
@Injectable()
export class HobbyService {

  constructor(public http: HttpClient) {
  }


  public getHobbyAll() {
    return this.http.get('http://localhost:8080/employee/skill/findAll', {}
    );
  }

  public getHobbyDetail(id: number) {
    return this.http.get('http://localhost:8080/employee/skill/findOne?id=:id', {}
    );
  }

  public deleteHobby(id: number) {
    return this.http.delete('http://localhost:8080/employee/skill/deleteOne?id='  + id, {headers:{'Content-Type': 'application/json'}},
    );
  }

  public saveHobby(hobby: Hobby) {
    return this.http.post('http://localhost:8080/employee/skill/save', JSON.stringify(hobby), {headers:{'Content-Type': 'application/json'}}
    );
  }
}
