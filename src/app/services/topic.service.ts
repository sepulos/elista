import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
/**
 * @author Damian Rabczewski,
 */
@Injectable()
export class TopicService {


  constructor(public http: HttpClient) {
  }


  public getTopicAll() {
    return this.http.get('http://localhost:8080/topic/findAll', {}
    );
  }

  public getTopicDetail(id: number) {
    return this.http.get('http://localhost:8080/topic/findOne?id=:id', {}
    );
  }

  public deleteTopic(id: number) {
    return this.http.delete('http://localhost:8080/topic/deleteOne?id=:id', {}
    );
  }

  public saveTopic() {
    return this.http.post('http://localhost:8080/topic/save', {}
    );
  }
}
