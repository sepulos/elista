import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Contact} from "../interfaces/contact";
/**
 * @author Damian Rabczewski,
 */
@Injectable()
export class ContactService {


  constructor(public http: HttpClient) {
  }


  public getContactAll() {
    return this.http.get('http://localhost:8080/employee/contact/findAll', {}
    );
  }

  public getContactDetail(id: number) {
    return this.http.get('http://localhost:8080/employee/contact/findOne?id=:id', {}
    );
  }

  public deleteContact(id: number) {
    return this.http.delete('http://localhost:8080/employee/contact/delete?id=' + id, {headers:{'Content-Type': 'application/json'}},
    );
  }

  public saveContact(contact: Contact) {
    return this.http.post('http://localhost:8080/employee/contact/save', JSON.stringify(contact), {headers:{'Content-Type': 'application/json'}}
    );
  }


}
