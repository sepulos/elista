import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {EmployeC} from "../interfaces/employe-c";
/**
 * @author Damian Rabczewski,
 */
@Injectable()
export class EmployeeService {

  constructor(public http: HttpClient) {
  }

  public getInit() {
    return this.http.get('http://localhost:8080/employee/init', {}
    );
  }

  public getInitContact() {
    return this.http.get('http://localhost:8080/employee/initContact', {}
    );
  }

  public getInitProject() {
    return this.http.get('http://localhost:8080/employee/initProject', {}
    );
  }

  public initSkill() {
    return this.http.get('http://localhost:8080/employee/initSkill', {}
    );
  }

  public getInitTimesheet() {
    return this.http.get('http://localhost:8080/employee/initTimesheet', {}
    );
  }

  public getInitTopic() {
    return this.http.get('http://localhost:8080/topic/init', {}
    );
  }

  public getInitPost() {
    return this.http.get('http://localhost:8080/post/init', {}
    );
  }

  public getEmployee() {
    return this.http.get('http://localhost:8080/employee/findAll', {}
    );
  }

  public getEmployeeDetail(id: number) {
    return this.http.get('http://localhost:8080/employee/findOne?id=' + id, {}
    );
  }

  public deleteEmployeeDetail(id: number) {
    return this.http.delete('http://localhost:8080/employee/deleteOne?id=:id', {}
    );
  }

  public saveEmployee(EmployeeDTO: EmployeC) {
    return this.http.post('http://localhost:8080/employee/save', JSON.stringify(EmployeeDTO), {headers:{'Content-Type': 'application/json'}}
    );
  }

}
