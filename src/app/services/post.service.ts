import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Post} from "../interfaces/post";
/**
 * @author Damian Rabczewski,
 */
@Injectable()
export class PostService {

  constructor(public http: HttpClient) {
  }


  public getPostAll() {
    return this.http.get('http://localhost:8080/post/findAll', {}
    );
  }

  public getPostDetail(id: number) {
    return this.http.get('http://localhost:8080/post/findOne?id=:id', {}
    );
  }

  public deletePost(id: number) {
    return this.http.delete('http://localhost:8080/post/deleteOne?id=:id', {}
    );
  }

  public savePost(news: Post) {
    return this.http.post('http://localhost:8080/post/save', JSON.stringify(news), {headers:{'Content-Type': 'application/json'}}
    );
  }
}
