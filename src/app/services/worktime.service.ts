import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Worktime} from "../interfaces/worktime";
/**
 * @author Damian Rabczewski,
 */
@Injectable()
export class WorktimeService {

  constructor(public http: HttpClient) {
  }


  public getWorktimeAll() {
    return this.http.get('http://localhost:8080/employee/timesheet/findAll', {}
    );
  }

  public getWorktimeDetail(id: number) {
    return this.http.get('http://localhost:8080/employee/timesheet/findOne?id=:id', {}
    );
  }

  public deleteWorktime(id: number) {
    return this.http.delete('http://localhost:8080/employee/timesheet/deleteOne?id=:id', {}
    );
  }

  public saveWorktime(work: Worktime) {
    return this.http.post('http://localhost:8080/employee/timesheet/save', JSON.stringify(work), {headers:{'Content-Type': 'application/json'}}
    );
  }
}
