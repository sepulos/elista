import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import { AccessComponent } from './components/access/access.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import { NotfoundComponent } from './pages/notfound/notfound.component';
import { UserPageComponent } from './pages/user-page/user-page.component';
import { DetailPageComponent } from './pages/detail-page/detail-page.component';
import { MenuComponent } from './components/menu/menu.component';
import {MaterialModule} from './modules/material.module';
import {EmployeeService} from './services/employee.service';
import {HttpClientModule} from '@angular/common/http';
import { DetailComponent } from './components/detail/detail.component';
import {AccordionModule} from 'primeng/accordion';
import {PanelModule} from 'primeng/panel';
import {MenuItem} from 'primeng/api';
import {ContactService} from "./services/contact.service";
import {ProjectService} from "./services/project.service";
import {HobbyService} from "./services/hobby.service";
import {WorktimeService} from "./services/worktime.service";
import {PostService} from "./services/post.service";
import {TopicService} from "./services/topic.service";
import { ListComponent } from './components/list/list.component';
import { RegisterComponent } from './components/register/register.component';
import { UpdateComponent } from './components/update/update.component';
import { MiddlesectionComponent } from './components/middlesection/middlesection.component';
import { NewsComponent } from './components/news/news.component';
import { OrganizerComponent } from './components/organizer/organizer.component';
import { ProjectComponent } from './components/project/project.component';

@NgModule({
  declarations: [
    AppComponent,
    AccessComponent,
    NotfoundComponent,
    UserPageComponent,
    DetailPageComponent,
    MenuComponent,
    DetailComponent,
    ListComponent,
    RegisterComponent,
    UpdateComponent,
    MiddlesectionComponent,
    NewsComponent,
    OrganizerComponent,
    ProjectComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    RouterModule.forRoot([
      {path: '', component: AccessComponent},
      {path: 'main', component: UserPageComponent},
      {path: 'user/:id', component: DetailPageComponent},
      {path: '**', component: NotfoundComponent}
    ]),
    AccordionModule,
    PanelModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [EmployeeService, ContactService, ProjectService, HobbyService, WorktimeService, PostService, TopicService],
  bootstrap: [AppComponent]
})
export class AppModule { }
