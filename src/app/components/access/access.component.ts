import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {IEmployees} from '../../interfaces/iemployees';
import {EmployeeService} from '../../services/employee.service';
import {User} from '../../interfaces/user';
import {Router} from '@angular/router';
import {ContactService} from "../../services/contact.service";
import {Contact} from "../../interfaces/contact";
import {ProjectService} from "../../services/project.service";
import {Project} from "../../interfaces/project";
import {Hobby} from "../../interfaces/hobby";
import {HobbyService} from "../../services/hobby.service";
import {Worktime} from "../../interfaces/worktime";
import {WorktimeService} from "../../services/worktime.service";
import {Post} from "../../interfaces/post";
import {PostService} from "../../services/post.service";
import {Topic} from "../../interfaces/topic";
import {TopicService} from "../../services/topic.service";
/**
 * @author Damian Rabczewski,
 */
declare var $: any;

@Component({
  selector: 'app-access',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AccessComponent implements OnInit {
  public user: User = {};
  arrayEmployee: Array<IEmployees>;
  arrayContact: Array<Contact>;
  arrayProject: Array<Project>;
  arrayHobby: Array<Hobby>;
  arrayWorktime: Array<Worktime>;
  arrayPost: Array<Post>;
  arrayTopic: Array<Topic>;
  id: string;
  useAnimate: boolean = false;

  constructor(private employeeService: EmployeeService,
              private contactService: ContactService,
              private projectService: ProjectService,
              private hobbyService: HobbyService,
              private worktimeService: WorktimeService,
              private postService: PostService,
              private topicService: TopicService,
              private router: Router) {
    this.employeeService.getEmployee().subscribe((res: Array<IEmployees>) => {
      this.arrayEmployee = res;
      // console.log(this.arrayEmployee);
    });
    // this.employeeService.getInit().subscribe();
    // this.employeeService.getInitContact().subscribe();
    // this.employeeService.getInitProject().subscribe();
    // this.employeeService.initSkill().subscribe();
    // this.employeeService.getInitTimesheet().subscribe();
    // this.employeeService.getInitTopic().subscribe();
    // this.employeeService.getInitPost().subscribe();
    // this.projectService.getProjectAll().subscribe((res: Array<Project>) => {
    //   this.arrayProject = res;
    //   console.log(this.arrayProject);
    // });
    // this.hobbyService.getHobbyAll().subscribe((res: Array<Hobby>) => {
    //   this.arrayHobby = res;
    //   console.log(this.arrayHobby);
    // });
    // this.worktimeService.getWorktimeAll().subscribe((res: Array<Worktime>) => {
    //   this.arrayWorktime = res;
    //   console.log(this.arrayWorktime);
    // });
    // this.postService.getPostAll().subscribe((res: Array<Post>) => {
    //   this.arrayPost = res;
    //   console.log(this.arrayPost);
    // });
    // this.topicService.getTopicAll().subscribe((res: Array<Topic>) => {
    //   this.arrayTopic = res;
    //   console.log(this.arrayTopic);
    // });
  }

  ngOnInit() {
  }



  public login(user: User) {
    for (let i = 0; i < this.arrayEmployee.length; i++) {
      if (this.arrayEmployee[i].login === user.login) {
        if (this.arrayEmployee[i].passwd === user.passwd) {
          this.id = this.arrayEmployee[i].id.toString();
          localStorage.setItem('token', this.id);
          localStorage.setItem('role', this.arrayEmployee[i].role);
          this.useAnimate = true;
          setTimeout(() => {
            this.router.navigate(['/main'])
            // console.log('timeout: ' + 1000);
          }, 1000);
          // console.log('asd');
        } else {
          // console.log('Błędny login lub hasło');
          // this.user = {};
        }
      } else {
        // console.log('Błędny login lub hasło');
        // this.user = {};
      }
    }
  }

}
