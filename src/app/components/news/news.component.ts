import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {PostService} from "../../services/post.service";
import {Post} from "../../interfaces/post";
import {Router} from "@angular/router";
import {MenuComponent} from "../menu/menu.component";
/**
 * @author Damian Rabczewski,
 */
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NewsComponent implements OnInit {

  news: Post = new Post;

  constructor(private postService: PostService, private router: Router,
              private menuComponent: MenuComponent) { }

  ngOnInit() {
  }

  public saveNews(){
    this.news.employeeId = parseInt(localStorage.getItem('token'),10);
    this.postService.savePost(this.news).subscribe(res => {
      this.menuComponent.showNews = false;
      this.router.navigate(['/main']);
    })
  }

}
