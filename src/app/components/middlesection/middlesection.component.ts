import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {PostService} from "../../services/post.service";
import {Post} from "../../interfaces/post";
import {IEmployees} from "../../interfaces/iemployees";
import {EmployeeService} from "../../services/employee.service";
import {Newsowner} from "../../interfaces/newsowner";
import {EmployeC} from "../../interfaces/employe-c";
import {HobbyService} from "../../services/hobby.service";
import {Hobby} from "../../interfaces/hobby";
import {Router, ActivatedRoute} from "@angular/router";
/**
 * @author Damian Rabczewski,
 */
@Component({
  selector: 'app-middlesection',
  templateUrl: './middlesection.component.html',
  styleUrls: ['./middlesection.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MiddlesectionComponent implements OnInit {

  listPost: Array<Post> = new Array<Post>();
  listEmployee: Array<IEmployees>;
  detailEmployee: EmployeC = new EmployeC;
  singleSortedNews: Newsowner = new Newsowner;
  listSortedNews: Array<Newsowner> = new Array<Newsowner>();
  msg: string;
  listHobby: Array<Hobby> = new Array<Hobby>();
  newHobby: Hobby = new Hobby;
  id: string;

  constructor(private employeeService: EmployeeService,
              private postService: PostService,
              private hobbyService: HobbyService,
              private router: Router,
              private route: ActivatedRoute) {
    this.listSortedNews = new Array<Newsowner>();
  }

  ngOnInit() {
    this.getNews();
    this.getEmployeeForNews();

  }

  public getNews() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      localStorage.setItem('showableUser', this.id);
      this.postService.getPostAll().subscribe((res: Array<Post>) => {
        this.listPost = new Array<Post>();
        this.listPost = res;
        // console.log(this.listPost);
      });
      this.getHobby();
      this.getEmployeeForNews();
    });
  }

  public getEmployeeDetail(id: number, msg: string) {
    this.employeeService.getEmployeeDetail(id).subscribe((res: EmployeC) => {
      this.singleSortedNews = new Newsowner;
      this.detailEmployee = res;
      // console.log(res);
      this.singleSortedNews.employee = this.detailEmployee;
      this.singleSortedNews.message = msg;
      // console.log(this.singleSortedNews);
      this.listSortedNews.push(this.singleSortedNews);
      // console.log(this.listSortedNews);
    });
  }

  public getEmployeeForNews() {
    this.listSortedNews = new Array<Newsowner>();
    this.employeeService.getEmployee().subscribe((res: Array<IEmployees>) => {
      this.listEmployee = res;
      // console.log(this.listEmployee);
      this.sortNewsEmployee();

    });
  }

  public sortNewsEmployee() {
    for (let i = 0; i < this.listPost.length; i++) {
      for (let j = 0; j < this.listEmployee.length; j++) {

        if (this.listPost[i].employeeId === this.listEmployee[j].id) {
          this.msg = '';
          // console.log(this.listPost[i].employeeId + ' asdasd' + this.listEmployee[j].id);
          this.singleSortedNews = new Newsowner;
          this.msg = this.listPost[i].message;
          this.getEmployeeDetail(this.listPost[i].employeeId, this.msg);
        }
      }
    }
    this.listSortedNews.sort().reverse();
  }

  public getHobby() {
    this.hobbyService.getHobbyAll().subscribe((res: Array<Hobby>) => {
      this.listHobby = new Array<Hobby>();
      for (let i = 0; i < res.length; i++) {
        if (res[i].employeeId === parseInt(localStorage.getItem('showableUser'), 10)) {
          this.listHobby.push(res[i]);
        }
      }
    });
  }

  public deleteHobby(id: number) {
    this.hobbyService.deleteHobby(id).subscribe();
    this.router.navigate(['/main']);
  }


  public saveHobby() {
    this.newHobby.employeeId = parseInt(localStorage.getItem('showableUser'), 10);
    this.hobbyService.saveHobby(this.newHobby).subscribe(res => {
      this.getHobby();
      this.newHobby = new Hobby;
    });
  }

}
