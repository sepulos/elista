import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {EmployeeService} from "../../services/employee.service";
import {EmployeC} from "../../interfaces/employe-c";
import {MenuComponent} from "../menu/menu.component";
/**
 * @author Damian Rabczewski,
 */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {

  newUser: EmployeC = new EmployeC;

  constructor(private employeeService: EmployeeService, private menuComponent: MenuComponent) {
  }

  ngOnInit() {
  }

  public saveNewAccount() {
    this.employeeService.saveEmployee(this.newUser).subscribe(res => {
      this.menuComponent.showRegister = false;
      // console.log("dodano");
    })
  }

}
