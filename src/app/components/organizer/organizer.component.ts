import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {WorktimeService} from "../../services/worktime.service";
import {Worktime} from "../../interfaces/worktime";
import {ProjectService} from "../../services/project.service";
import {Project} from "../../interfaces/project";
import {ActivatedRoute} from "@angular/router";
/**
 * @author Damian Rabczewski,
 */
@Component({
  selector: 'app-organizer',
  templateUrl: './organizer.component.html',
  styleUrls: ['./organizer.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class OrganizerComponent implements OnInit {
  arrayWorktime: Array<Worktime> = new Array<Worktime>();
  currentWorktime: Array<Worktime> = new Array<Worktime>();
  idp: number = null;
  newWorktime: Worktime = new Worktime;

  public listProject: Array<Project> = new Array<Project>();
  public detailUserlistProject: Array<Project> = new Array<Project>();
  newProject: Project = new Project;
  id: string;

  constructor(private worktimeService: WorktimeService, private projectService: ProjectService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.getWorktime();
    this.getProjects();
  }

  public getWorktime() {
    this.worktimeService.getWorktimeAll().subscribe((res: Array<Worktime>) => {
      this.route.params.subscribe(params => {
        this.id = params['id'];
        localStorage.setItem('showableUser', this.id);
        this.getProjects();
        this.workForEmployee(res);
      });

      // console.log('ziom' + this.currentWorktime);
    });
  }

  public workForEmployee(res) {
    this.idp = parseInt(this.id, 10);
    this.arrayWorktime = [];
    this.currentWorktime = [];
    this.arrayWorktime = res;
    for (let i = 0; i < this.arrayWorktime.length; i++) {
      if (this.arrayWorktime[i].employeeId === this.idp) {
        this.currentWorktime.push(this.arrayWorktime[i]);
      }
    }
  }


  public saveNewWork() {
    this.newWorktime.employeeId = parseInt(localStorage.getItem('showableUser'), 10);
    this.newWorktime.workplace = 'Biuro';
    // console.log(new Date(this.newWorktime.workDate).getTime());
    this.newWorktime.workDate = new Date(this.newWorktime.workDate).getTime();
    this.worktimeService.saveWorktime(this.newWorktime).subscribe(res => {
      this.getWorktime();
      this.newWorktime = new Worktime;
    });
  }


  public getProjects() {
    this.listProject = [];
    this.detailUserlistProject = [];
    this.projectService.getProjectAll().subscribe((res: Array<Project>) => {
      this.listProject = res;
      // console.log(this.listProject);
      this.sortProject();
    });
  }

  // public deleteProject(id: number) {
  //   this.projectService.deleteProject(id).subscribe();
  //   this.router.navigate(['/main']);
  // }

  private sortProject() {
    var projectId = parseInt(localStorage.getItem('showableUser'), 10);
    for (let i = 0; i < this.listProject.length; i++) {
      // console.log(this.listProject[i]);
      if (this.listProject[i].employeeId === projectId) {
        // console.log('super');
        this.detailUserlistProject.push(this.listProject[i]);
      }
    }
  }

}
