import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {EmployeeService} from "../../services/employee.service";
import {IEmployees} from "../../interfaces/iemployees";
import {MenuComponent} from "../menu/menu.component";
/**
 * @author Damian Rabczewski,
 */
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ListComponent implements OnInit {

  listEmployee: Array<IEmployees>;

  constructor(private employeeService: EmployeeService, private menuComponent: MenuComponent) {
    this.employeeService.getEmployee().subscribe((res: Array<IEmployees>) => {
      this.listEmployee = res;
      // console.log(this.listEmployee);
    });
  }

  ngOnInit() {
  }

  public closeList(){
    this.menuComponent.showList = false;
  }

}
