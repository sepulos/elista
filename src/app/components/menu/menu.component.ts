import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {IEmployees} from "../../interfaces/iemployees";
import {EmployeeService} from "../../services/employee.service";
/**
 * @author Damian Rabczewski,
 */
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MenuComponent implements OnInit {
  inputContainerVisible = false;
  public refreshListAfterAdd: ($event) => void;
  public employeDetail: IEmployees;
  public showAdminFunction: boolean = false;
  public showList: boolean = false;
  public showRegister: boolean = false;
  public showUpdate: boolean = false;
  public showNews: boolean = false;
  public showProject: boolean = false;
  admin: boolean = false;
  yourAccount: boolean = false;
  constructor(private employeeService: EmployeeService) {
    this.employeeService.getEmployeeDetail(1).subscribe(this.getEmployeeCallBack);
    if(localStorage.getItem('role') === 'Admin'){
      this.admin = true;
    } else {
      this.admin = false;
    }
    if(localStorage.getItem('showableUser') === localStorage.getItem('token')){
      this.yourAccount = true;
    } else {
      this.yourAccount = false;
    }
  }

  ngOnInit() {
  }

  private getEmployeeCallBack = (res: IEmployees) => {
    this.employeDetail = res;
    if(this.employeDetail.role === 'Admin') {
      this.showAdminFunction = true;
    } else {
      this.showAdminFunction = false;
    }
  }



}
