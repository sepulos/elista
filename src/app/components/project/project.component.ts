import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {Project} from "../../interfaces/project";
import {ProjectService} from "../../services/project.service";
import {MenuComponent} from "../menu/menu.component";
/**
 * @author Damian Rabczewski,
 */
@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProjectComponent implements OnInit {
  newProject: Project = new Project;
  constructor(private projectService: ProjectService, private menuComponent: MenuComponent) { }

  ngOnInit() {
  }

  public saveNewProject() {
    this.projectService.saveProject(this.newProject).subscribe(res => {
      this.menuComponent.showProject = false;
      // console.log("dodano");
    })
  }
}
