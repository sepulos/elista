import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {EmployeC} from "../../interfaces/employe-c";
import {EmployeeService} from "../../services/employee.service";
import {MenuComponent} from "../menu/menu.component";
/**
 * @author Damian Rabczewski,
 */
@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UpdateComponent implements OnInit {
  currentUser: EmployeC = new EmployeC;
  selected: any;
  admin: boolean = false;
  yourAccount: boolean = false;
  constructor(private employeeService: EmployeeService, private menuComponent: MenuComponent) {
    this.employeeService.getEmployeeDetail(parseInt(localStorage.getItem('showableUser'), 10)).subscribe((res: EmployeC) => {
      this.currentUser = res;
    })
    if(localStorage.getItem('role') === 'Admin'){
      this.admin = true;
    } else {
      this.admin = false;
    }
    if(localStorage.getItem('showableUser') === localStorage.getItem('token')){
      this.yourAccount = true;
    } else {
      this.yourAccount = false;
    }
  }

  ngOnInit() {
  }

  public updateAccount() {
    this.employeeService.saveEmployee(this.currentUser).subscribe(res => {
      this.menuComponent.showUpdate = false;
      // console.log("update");
    })
  }
}
