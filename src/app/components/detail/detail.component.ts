import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {EmployeeService} from "../../services/employee.service";
import {IEmployees} from "../../interfaces/iemployees";
import {ActivatedRoute, Router} from "@angular/router";
import {EmployeC} from "../../interfaces/employe-c";
import {ContactService} from "../../services/contact.service";
import {Contact} from "../../interfaces/contact";
/**
 * @author Damian Rabczewski,
 */
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DetailComponent {
  public detail: EmployeC = new EmployeC;
  public listContact: Array<Contact> = new Array<Contact>();
  public detailUserlistContact: Array<Contact> = new Array<Contact>();
  newContact: Contact = new Contact;
  // id: number = parseInt(localStorage.getItem('showDetailUser'), 10);
  id: string;
  admin: boolean = false;
  yourAccount: boolean = false;

  constructor(private employeeService: EmployeeService, private router: Router,
              private contactService: ContactService, private route: ActivatedRoute) {
    this.getEmployee();
    if (localStorage.getItem('role') === 'Admin') {
      this.admin = true;
    } else {
      this.admin = false;
    }
    if (localStorage.getItem('showableUser') === localStorage.getItem('token')) {
      this.yourAccount = true;
    } else {
      this.yourAccount = false;
    }
  }

  private init() {
    this.getEmployee();

  }

  public getEmployee() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      localStorage.setItem('showableUser', this.id);
      // console.log(this.id + '54654');
      this.employeeService.getEmployeeDetail(parseInt(this.id, 10)).subscribe((res: IEmployees) => {
        this.detail = res;
        // console.log(this.detail);
      });
      this.getContacts();
    });
  }

  public getContacts() {
    this.listContact = [];
    this.detailUserlistContact = [];
    this.contactService.getContactAll().subscribe((res: Array<Contact>) => {
      this.listContact = res;
      // console.log(this.listContact);
      this.sortContact();
  });
  }

  public deleteContact(id: number) {
    this.contactService.deleteContact(id).subscribe();
    this.router.navigate(['/main']);
  }

  private sortContact() {
    var contactId = parseInt(localStorage.getItem('showableUser'), 10);
    for (let i = 0; i < this.listContact.length; i++) {
      // console.log(this.listContact[i]);
      if (this.listContact[i].employeeId === contactId) {
        // console.log('super');
        this.detailUserlistContact.push(this.listContact[i]);
      }
    }
  }


  public saveNewContact() {
    this.newContact.employeeId = parseInt(localStorage.getItem('showableUser'), 10);
    this.contactService.saveContact(this.newContact).subscribe(res => {
      this.getContacts();
      this.newContact = new Contact;
    });
  }

}
