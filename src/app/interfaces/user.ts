/**
 * @author Damian Rabczewski,
 */
export interface User {
  passwd?: string;
  login?: string;
  employeeId?: number;
}
