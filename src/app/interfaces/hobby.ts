/**
 * @author Damian Rabczewski,
 */
export class Hobby {
  id: number;
  skillName: string;
  description: string;
  employeeId: number;

  constructor(){

    this.id = null;
    this.skillName = '';
    this.description = '';
    this.employeeId = null;
  }
}
