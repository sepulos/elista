/**
 * @author Damian Rabczewski,
 */
export interface Topic {
  id: number;
  message: string;
  senderEmployeeId: number;
  employeeId: number;
}
