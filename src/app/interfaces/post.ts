/**
 * @author Damian Rabczewski,
 */
export class Post {
  id: number;
  message: string;
  employeeId: number;

  constructor(){

    this.id = null;
    this.message = '';
    this.employeeId = null;
  }
}
