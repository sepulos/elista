/**
 * @author Damian Rabczewski,
 */
import {EmployeC} from "./employe-c";

export class Newsowner {
  employee: EmployeC;
  message: string;

  constructor() {
    this.employee = new EmployeC;
    this.message = '';
  }

}
