/**
 * @author Damian Rabczewski,
 */
export interface IEmployees {
  id: number;
  name: string;
  lastname: string;
  position: string;
  avatarPath: string;
  role: string;
  cashPerHour: number;
  passwd: string;
  login: string;
  aboutPerson: string;
}
