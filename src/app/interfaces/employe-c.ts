/**
 * @author Damian Rabczewski,
 */
export class EmployeC {
  id: number;
  name: string;
  lastname: string;
  position: string;
  avatarPath: string;
  role: string;
  cashPerHour: number;
  passwd: string;
  login: string;
  aboutPerson: string;

  constructor() {
    this.id = null;
    this.name = '';
    this.lastname = '';
    this.position = '';
    this.avatarPath = '';
    this.role = '';
    this.cashPerHour = null;
    this.passwd = '';
    this.login = '';
    this.aboutPerson = '';
  }

}
