/**
 * @author Damian Rabczewski,
 */
export class Worktime {
  id: number;
  workplace: string;
  workDate: number;
  startDate: number;
  task: string;
  employeeId: number;

  constructor() {

    this.id = null;
    this.workplace = '';
    this.workDate = null;
    this.startDate = null;
    this.task = '';
    this.employeeId = null;
  }
}
