/**
 * @author Damian Rabczewski,
 */
export class Contact {
  id: number;
  contactType: string;
  contactValue: string;
  employeeId: number;

  constructor() {
    this.id = null;
    this.contactType = '';
    this.contactValue = '';
    this.employeeId = null;
  }
}
