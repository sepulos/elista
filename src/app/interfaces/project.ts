/**
 * @author Damian Rabczewski,
 */
export class Project {
  id: number;
  projectName: string;
  status: string;
  employeeId: number;

  constructor(){

    this.id = null;
    this.projectName = '';
    this.status = '';
    this.employeeId = null;
  }
}
