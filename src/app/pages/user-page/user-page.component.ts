import { Component, OnInit, ViewEncapsulation } from '@angular/core';
/**
 * @author Damian Rabczewski,
 */
@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
