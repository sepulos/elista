import { Component, OnInit, ViewEncapsulation } from '@angular/core';
/**
 * @author Damian Rabczewski,
 */
@Component({
  selector: 'app-notfound',
  templateUrl: './notfound.component.html',
  styleUrls: ['./notfound.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NotfoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
