import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {EmployeeService} from "../../services/employee.service";
/**
 * @author Damian Rabczewski,
 */
@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DetailPageComponent implements OnInit {

  id: number;

  constructor(private route: ActivatedRoute) {
    console.log(this.route);
    this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log(this.id);
    });
  }

  ngOnInit() {
  }

}
